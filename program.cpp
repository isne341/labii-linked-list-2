#include <iostream>
#include "list.h"
#include "list.cpp"
using namespace std;

void main()
{
	//Sample Code
	List<int> mylist;
	mylist.pushToHead(2);
	mylist.pushToHead(1);
	mylist.pushToHead(3);
	cout << "Linked list of int: ";
	mylist.print();
	cout << endl << "Popping tail: " << mylist.popTail() * 1 << endl;
	if(mylist.search(1))cout << "1 is found\n";

	List<char> mylist2;
	mylist2.pushToTail('k');
	mylist2.pushToTail('e');
	mylist2.pushToTail('n');
	cout << "Linked list of char: ";
	mylist2.print();
	cout << endl << "Popping head: " << mylist2.popHead() << endl;
	if(mylist2.search('e'))cout << "e is found\n";

	//TO DO! Complete the functions, then write a program to test them.
	//Adapt your code so that your list can store data other than characters - how about a template?
	
}