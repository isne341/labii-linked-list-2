#include <iostream>
#include "list.h"
using namespace std;
template <class T>
List<T>::~List() {
	for(Node<T> *p; !isEmpty(); ) {
		p=head->next;
		delete head;
		head = p;
	}
}
template <class T>
void List<T>::pushToHead(T el)
{
	head = new Node<T>(el, head, 0);
	if(tail==0){
		tail = head;
	}
	else{	
		head->next->prev = head;
	}
}
template <class T>
void List<T>::pushToTail(T el)
{
	tail = new Node<T>(el, 0, tail);
	if(head == 0){
		head = tail;
	}
	else{
		tail->prev->next = tail;
	}
	
}
template <class T>
char List<T>::popHead()
{
	T el = head->data;
	Node<T> *tmp = head;
	if(head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
template <class T>
char List<T>::popTail()
{
	T el = tail->data;
	Node<T> *tmp = tail;
	if(tail == head){
		tail = head = 0;
	}
	else{
		tail = tail->prev;
		tail->next = 0;
	}
	delete tmp;
	return el;
}
template <class T>
bool List<T>::search(T el)
{
	// TO DO! (Function to return True or False depending if a character is in the list.
	Node<T> *tmp = head;
	while(true){

		if(tmp->data == el)return true;
		
		tmp = tmp->next;
		
		if(tmp == 0)break;
	}
	return false;
}
template <class T>
void List<T>::print()
{
	if(head  == tail)
	{
		cout << head->data;
	}
	else
	{
		Node<T> *tmp = head;
		while(tmp!=tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}
